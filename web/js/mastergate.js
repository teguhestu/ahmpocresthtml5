var rootURL = "http://localhost:7001/AHMPOCRestEE5/rest/mastergate";

var currentMastergate;

// Retrieve mastergate list when application starts 
findAll();

// Nothing to delete in initial application state
$('#btnDelete').hide();

// Register listeners
$('#btnSearch').click(function() {
	search($('#searchKey').val());
	return false;
});

// Trigger search when pressing 'Return' on search key input field
$('#searchKey').keypress(function(e){
	if(e.which == 13) {
		search($('#searchKey').val());
		e.preventDefault();
		return false;
    }
});

$('#btnAdd').click(function() {
	newMastergate();
	return false;
});

$('#btnSave').click(function() {
	if ($('#mastergateId').val() == '')
		addMastergate();
	else
		updateMastergate();
	return false;
});

$('#btnDelete').click(function() {
	deleteMastergate();
	return false;
});

$('#mastergateList a').live('click', function() {
	findById($(this).data('identity'));
});

// Replace broken images with generic mastergate bottle
$("img").error(function(){
  $(this).attr("src", "pics/generic.jpg");

});

function search(searchKey) {
	if (searchKey == '') 
		findAll();
	else
		findByName(searchKey);
}

function newMastergate() {
	$('#btnDelete').hide();
	currentMastergate = {};
	renderDetails(currentMastergate); // Display empty form
}

function findAll() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

function findByName(searchKey) {
	console.log('findByName: ' + searchKey);
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + searchKey,
		dataType: 'json',
		success: renderList 
	});
}

function findById(id) {
	console.log('findById: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
			console.log('findById success: ' + data.name);
			currentMastergate = data;
			renderDetails(currentMastergate);
		}
	});
}

function addMastergate() {
	console.log('addMastergate');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Mastergate created successfully');
			$('#btnDelete').show();
			$('#mastergateId').val(data.id);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('addMastergate error: ' + textStatus);
		}
	});
}

function updateMastergate() {
	console.log('updateMastergate');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#mastergateId').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Mastergate updated successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updateMastergate error: ' + textStatus);
		}
	});
}

function deleteMastergate() {
	console.log('deleteMastergate');
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#mastergateId').val(),
		success: function(data, textStatus, jqXHR){
			alert('Mastergate deleted successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('deleteMastergate error');
		}
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data.result == null ? [] : (data.result instanceof Array ? data.result : [data.result]);

	$('#mastergateList li').remove();
	$.each(list, function(index, mastergate) {
		$('#mastergateList').append('<li><a href="#" data-identity="' + mastergate.vgateid + '">'+mastergate.vgatedesc+'</a></li>');
	});
}

function renderDetails(mastergate) {
	$('#mastergateId').val(mastergate.id);
	$('#name').val(mastergate.name);
	$('#grapes').val(mastergate.grapes);
	$('#country').val(mastergate.country);
	$('#region').val(mastergate.region);
	$('#year').val(mastergate.year);
	$('#pic').attr('src', 'pics/' + mastergate.picture);
	$('#description').val(mastergate.description);
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	var mastergateId = $('#mastergateId').val();
	return JSON.stringify({
		"id": mastergateId == "" ? null : mastergateId, 
		"name": $('#name').val(), 
		"grapes": $('#grapes').val(),
		"country": $('#country').val(),
		"region": $('#region').val(),
		"year": $('#year').val(),
		"picture": currentMastergate.picture,
		"description": $('#description').val()
		});
}/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


